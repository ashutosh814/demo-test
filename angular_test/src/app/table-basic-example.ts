import {Component} from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';

export interface PeriodicElement {
  email: string;
  position: number;
  order: number;
  type: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {position: 1, email: 'abc@gmail.com', order: 10079, type: 'Inventry'},
  { position: 2, email: 'bcd@gmail.com', order: 40026, type: 'Inventry'},
  { position: 3, email: 'kk@gmail.com', order: 6941, type: 'Inventry'},
  { position: 4, email: 'mm@gmail.com', order: 90122, type: 'Inventry'},
 
];

/**
 * @title Basic use of `<table mat-table>`
 */
@Component({
  selector: 'table-basic-example',
  styleUrls: ['table-basic-example.css'],
  templateUrl: 'table-basic-example.html',
})
export class TableBasicExample {

  displayedColumns: string[] = ['position', 'email', 'order', 'type'];
  dataSource = new MatTableDataSource(ELEMENT_DATA);

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}






