const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const cors = require('cors');
const path = require('path');
const { admin, password, port } = require('../src/app/helpers/config');
const userRoute = require('./app/route/user_route');


const app = express();
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(express.static(path.join(__dirname, '../uploads')));

app.use('/api/user', userRoute);



let url = "mongodb://localhost:27017/demo-node";

//let url = `mongodb://${admin}:${password}@ip_server:27017/demo-node`;

//let url = `mongodb://${admin}:${password}@localhost:27017/demo-node`;


//For adding admin panel
app.use('/angular_file', express.static(path.join(__dirname, 'dist', 'angular_file')));
app.get('/angular_file*', (req, res) => {
    res.sendFile(`${__dirname}/dist/angular_file/index.html`);
})



mongoose.connect(url, (err) => {
    if (err) {
        console.log("Error is : " + err);
    } else {
        console.log("MongoDb is successfully connected....", url);
    }
});


app.listen(port, () => {
    console.log(`Server is connected at port ${port}....`);
});
