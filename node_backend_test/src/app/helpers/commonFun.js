const randomstring = require("randomstring");

exports.randomStringGenerator = () => {
    let text = "";
    let possible = "123456789";

    for (let i = 0; i < 4; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return "1234"
};

exports.randomOrderIdGenerator = () => {
    let text = "";
    let possible = "123456789";
    let str = randomstring.generate(6);

    for (let i = 0; i < 8; i++) {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    };
    let res = `ORD${text}${str}`;
    return res;
};
