const { saving_event, userList  } = require('../controller/user_controller');

const express = require('express');
const route = express.Router();


route.post('/saving_event', saving_event);
route.get('/user-list', userList);

module.exports = route;