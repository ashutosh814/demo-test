const { users } = require("../models/user-model");
const { Order } = require("../models/order-model");
const config = require('../helpers/config'); // get config file
const { msg } = require('../helpers/message');
const { ObjectId } = require('bson');


let saving_event = async (req) => {
    let user_resut ={}
    let { Email, Environment, message, payload_data, component} = req.body;
    
    let update_order_data = new Order(payload_data);
    let result = await update_order_data.save();

    if(result){
        let user = new users({ Email, Environment, message, component, order_id: result._id});
        user_resut = await user.save();
        user_resut.payload_data = payload_data;
    }

    return {
        response: user_resut,
        message: msg.success
    };
};


//I want to retrieve the activity of abc@gmail.com from 1-1-2018 on the environment
//“production” on the component “inventory” that contains the text “error”

let userList = async (req) => {
    let { email, Environment, component,message}= req.body;
    let res = await users.find({ email, Environment, component, message: { $regex: message, $options: 'i' }}).lean().sort({ _id: -1 });
    if (!res || res == null) throw { message: msg.notExist };
    return {
        response: res,
        message: msg.success
    }
};

module.exports = {
    saving_event, userList
};