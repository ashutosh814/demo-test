const { saving_event, userList } = require('../business/user_business');

exports.saving_event = async (req, res) => {
    try {
        let r = await saving_event(req);
        res.status(200).send(r);
    } catch (err) {
        console.log("Error is : " + err);
        res.status(400).send(err);
    }
};



exports.userList = async (req, res) => {
    try {
        let r = await userList(req);
        res.status(200).send(r);
    } catch (err) {
        console.log("Error is : " + err);
        res.status(400).send(err);
    }
};

