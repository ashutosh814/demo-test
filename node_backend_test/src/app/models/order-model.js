const mongoose = require("mongoose");

const OrderSchema = new mongoose.Schema({
    CreatedAt: {
        type: Number,
        default: new Date().getTime()
    },
    order_id: {
        type: String,
        require: true
    },
    amount:{
        type:Number
    },
    Order_type:{
        type:Number
    },
    num_of_item:{
        type:Number
    }


},
    {
        versionKey: false,
        timestamps: true
    }
);

const Order = mongoose.model('Order', OrderSchema);

module.exports = { Order };