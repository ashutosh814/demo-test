const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
        CreatedAt:{
            type:Number,
            default:new Date().getTime()
        },
        Email:{
            type:String,
            require:true
        },
        Environment:{
            type:Number  // 1 for local 2 for producation
        },
        message:{
            type:String
        },
        payload_data:{
            type:JSON
        },
        component:{
            type:String
        },
        order_id:{
            type: mongoose.Schema.ObjectId,
            ref: "Order",
        }

},
    {
        versionKey: false,
        timestamps: true
    }
);

const users = mongoose.model('users', userSchema);

module.exports = { users };